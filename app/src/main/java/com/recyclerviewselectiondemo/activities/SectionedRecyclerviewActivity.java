package com.recyclerviewselectiondemo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.recyclerviewselectiondemo.R;

public class SectionedRecyclerviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sectioned_recyclerview);
    }
}
