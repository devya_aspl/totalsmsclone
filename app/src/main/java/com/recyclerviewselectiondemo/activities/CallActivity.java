package com.recyclerviewselectiondemo.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.recyclerviewselectiondemo.recyclerview_selection.ContactItemDetailsLookup;
import com.recyclerviewselectiondemo.recyclerview_selection.ContactItemKeyProvider;
import com.recyclerviewselectiondemo.recyclerview_selection.ContactListAdapter;
import com.recyclerviewselectiondemo.Model.ContactModel;
import com.recyclerviewselectiondemo.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import butterknife.BindView;
import butterknife.ButterKnife;


public class CallActivity extends AppCompatActivity implements Switch.OnCheckedChangeListener,Spinner.OnItemSelectedListener {

    RecyclerView rv_contact_list,rv_display;
    ArrayList<ContactModel> data;
    ArrayList<ContactModel> selectedData;
    TextView tvSelectionCount,tv_select_all;
    Context context;
    ContactListAdapter contactListAdapter;
    Switch switch_select_all;
    private SelectionTracker mSelectionTracker;
    ArrayAdapter<String> adapterChooseSortBy,adapterChooseSortByTier;

    @BindView(R.id.sp_sort_country)
    Spinner sp_sort_country;

    @BindView(R.id.sp_sort_tier)
    Spinner sp_sort_tier;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        context = CallActivity.this;
        ButterKnife.bind(this);
        rv_contact_list = findViewById(R.id.rv_contact_list);
        tvSelectionCount = findViewById(R.id.tvSelectionCount);
        tv_select_all = findViewById(R.id.tv_select_all);
        switch_select_all = findViewById(R.id.switch_select_all);
        rv_display = findViewById(R.id.rv_display);
        switch_select_all.setEnabled(false);
//        if (mSelectionTracker.hasSelection())
//        {
        switch_select_all.setOnCheckedChangeListener(this);
//        }
        setUpList();
        setUpSpinners();
        try {
            mSelectionTracker = new SelectionTracker.Builder<>(
                    "string-items-selection",
                    rv_contact_list,
                    new ContactItemKeyProvider(1, data),
                    new ContactItemDetailsLookup(rv_contact_list),
                    StorageStrategy.createParcelableStorage(ContactModel.class)
            ).withSelectionPredicate(SelectionPredicates.<ContactModel>createSelectAnything())
                    .build();

            contactListAdapter.setSelectionTracker(mSelectionTracker);

            mSelectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                @Override
                public void onItemStateChanged(@NonNull Object key, boolean selected) {
                    super.onItemStateChanged(key, selected);
//                Log.e("ItemStateChanged %s to %b", key, selected);
                }

                @Override
                public void onSelectionRefresh() {
                    super.onSelectionRefresh();
                    tvSelectionCount.setText("Selection Count: 0");
                }

                @Override
                public void onSelectionChanged() {
                    super.onSelectionChanged();
                    switch_select_all.setEnabled(true);
//                    mSelectionTracker.select()
                    if (mSelectionTracker.hasSelection()) {
                        Log.e("activity", "selected Element: " + mSelectionTracker.getSelection().toString());
                        tvSelectionCount.setText(String.format("Selection Count: %d", mSelectionTracker.getSelection().size()));
                    } else {
                        tvSelectionCount.setText("Selection Count: 0");
                    }

                    Iterator<ContactModel> itemIterable = mSelectionTracker.getSelection().iterator();
                    List<ContactModel> mySelectedList = copyIterator(itemIterable);
                    Log.e("activity","Selected Arraylist: "+mySelectedList.size());
                    while (itemIterable.hasNext()) {
                        try {
                            Log.e("activity", itemIterable.next().getContactNumber());
                            Log.e("activity", itemIterable.next().getContactCountry());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onSelectionRestored() {
                    super.onSelectionRestored();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        tv_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAll();
            }
        });

    }

    private void setUpSpinners() {
        List<String> spinnerChooseSortBy=  new ArrayList<String>();
        spinnerChooseSortBy.add("All Countries");
        spinnerChooseSortBy.add("India");
        spinnerChooseSortBy.add("America");
        spinnerChooseSortBy.add("Ukraine");
        spinnerChooseSortBy.add("USA");

        adapterChooseSortBy = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerChooseSortBy);

        adapterChooseSortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
        sp_sort_country.setAdapter(adapterChooseSortBy);

        sp_sort_country.setOnItemSelectedListener(this);

        List<String> spinnerChooseSortByTier=  new ArrayList<String>();
        spinnerChooseSortByTier.add("All Tier");
        spinnerChooseSortByTier.add("Tier 1");
        spinnerChooseSortByTier.add("Tier 2");
        spinnerChooseSortByTier.add("Tier 3");
        spinnerChooseSortByTier.add("Tier 4");

        adapterChooseSortByTier = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerChooseSortByTier);

        adapterChooseSortByTier.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
        sp_sort_tier.setAdapter(adapterChooseSortByTier);

        sp_sort_tier.setOnItemSelectedListener(this);
    }

    public static <ContactModel> List<ContactModel> copyIterator(Iterator<ContactModel> iter) {
        List<ContactModel> copy = new ArrayList<ContactModel>();
        while (iter.hasNext())
            copy.add(iter.next());
        return copy;
    }

    private void setUpList() {
        rv_contact_list.setLayoutManager(new LinearLayoutManager(context));

        data = new ArrayList<ContactModel>();
        data.add(new ContactModel(1,"+91 7043169995","India"));
        data.add(new ContactModel(2,"+91 8154936235","India"));
        data.add(new ContactModel(3,"+91 8160058122","America"));
        data.add(new ContactModel(4,"+91 8154936235","Ukraine"));
        data.add(new ContactModel(5,"+91 8160058122","USA"));
        data.add(new ContactModel(6,"+91 8154936235","India"));
        data.add(new ContactModel(7,"+91 7043169995","India"));
        data.add(new ContactModel(8,"+91 8154936235","Ukraine"));
        data.add(new ContactModel(9,"+91 8160058122","USA"));
        data.add(new ContactModel(10,"+91 8154936235","India"));

        contactListAdapter = new ContactListAdapter(context,data);

        Log.e("Before Sorting:",data.get(0).getContactNumber());

        rv_contact_list.setAdapter(contactListAdapter);

//        Collections.sort(callModelList, new Comparator<CallModel>() {
//            public int compare(CallModel v1, CallModel v2) {
//                return v1.getCountry().compareTo(v2.getCountry());
//            }
//        });
//
//        Log.e("Sorting:",callModelList.get(0).getCountry());


//        callModelList.sort();
    }

    private void selectAll()
    {
        if (mSelectionTracker.hasSelection())
        {
            mSelectionTracker.setItemsSelected(data, true);
        }
    }

    private void unSelectAll()
    {
        if (mSelectionTracker.hasSelection())
        {
            mSelectionTracker.clearSelection();
        }
        switch_select_all.setEnabled(false);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (mSelectionTracker.hasSelection())
        {
            if (b)
            {
                selectAll();
            }
            else
            {
                unSelectAll();
            }
//            mSelectionTracker.setItemsSelected(data, true);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (!sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("All Countries"))
        {
            switch_select_all.setEnabled(false);
            filter(sp_sort_country.getSelectedItem().toString());
//            setUpList();
        }
//        if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("India"))
        else
        {
            switch_select_all.setEnabled(false);
//            ArrayList<ContactModel> filteredList = new ArrayList<>();
            contactListAdapter.filterList(data);
//            contactListAdapter.notifyDataSetChanged();
        }
    }

    private void filter(String text) {

        try{
            ArrayList<ContactModel> filteredList = new ArrayList<>();

            for (ContactModel item : data)
            {
                if (item.getContactCountry().toLowerCase().contains(text.toLowerCase()))
                {
                    filteredList.add(item);
                }
            }

            contactListAdapter.filterList(filteredList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
