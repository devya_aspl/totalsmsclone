package com.recyclerviewselectiondemo.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactModel implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };

    int id;
    String contactNumber,contactCountry;

    public ContactModel(int id, String contactNumber, String contactCountry) {
        this.id = id;
        this.contactNumber = contactNumber;
        this.contactCountry = contactCountry;
    }

    public String getContactCountry() {
        return contactCountry;
    }

    public void setContactCountry(String contactCountry) {
        this.contactCountry = contactCountry;
    }

    public ContactModel(int id, String contactNumber) {
        this.id = id;
        this.contactNumber = contactNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//
//    }

    // Parcelling part
    public ContactModel(Parcel in){
        this.id = in.readInt();
        this.contactCountry = in.readString();
        this.contactNumber =  in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.contactCountry);
        dest.writeString(this.contactNumber);
    }

    @Override
    public String toString() {
        return "ContactModel{" +
                "id='" + id + '\'' +
                ", contactCountry='" + contactCountry + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                '}';
    }
}
