package com.recyclerviewselectiondemo.recyclerview_selection;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.recyclerviewselectiondemo.Model.ContactModel;
import com.recyclerviewselectiondemo.R;

import java.util.ArrayList;

import androidx.recyclerview.selection.SelectionTracker;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    Context context;
    ArrayList<ContactModel> data;

    private SelectionTracker mSelectionTracker;

    public ContactListAdapter(Context context, ArrayList<ContactModel> data) {
        this.context = context;
        this.data = data;
    }

    public void setSelectionTracker(SelectionTracker mSelectionTracker) {
        this.mSelectionTracker = mSelectionTracker;
    }

    @NonNull
    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactListAdapter.ViewHolder holder, int position) {
        try {
            ContactModel item = data.get(position);
            holder.bind(item, mSelectionTracker.isSelected(item));
            holder.text_view_country.setText(data.get(position).getContactCountry());
            holder.tv_number.setText(data.get(position).getContactNumber());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void filterList(ArrayList<ContactModel> filteredData) {
        data = filteredData;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout ll_call_list_item;
        TextView tv_number,text_view_country;

        ViewHolder(@NonNull View v) {
            super(v);
            ll_call_list_item = (LinearLayout)v.findViewById(R.id.ll_call_list_item);
            tv_number = (TextView)v.findViewById(R.id.tv_number);
            text_view_country = (TextView)v.findViewById(R.id.text_view_country);

        }

        void bind(ContactModel item, boolean isSelected) {
//            textView.setText(item);
            // If the item is selected then we change its state to activated
            ll_call_list_item.setActivated(isSelected);
        }

//        /**
//         * Create a new {@link StringItemDetails} for each string item, will be used later by {@link StringItemDetailsLookup#getItemDetails(MotionEvent)}
//         * @return {@link StringItemDetails} instance
//         */
        ContactItemDetails getItemDetails() {
            return new ContactItemDetails(getAdapterPosition(), data.get(getAdapterPosition()));
        }
    }
}
