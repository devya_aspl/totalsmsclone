package com.recyclerviewselectiondemo.recyclerview_selection;

import android.support.annotation.Nullable;

import com.recyclerviewselectiondemo.Model.ContactModel;

import androidx.recyclerview.selection.ItemDetailsLookup;

/**
 * An {@link ItemDetailsLookup.ItemDetails} that holds details about a {@link String} item like its position and its value.
 */

public class ContactItemDetails extends ItemDetailsLookup.ItemDetails {

    private int position;
    private ContactModel item;

    public ContactItemDetails(int position, ContactModel item) {
        this.position = position;
        this.item = item;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Nullable
    @Override
    public Object getSelectionKey() {
        return item;
    }
}
