package com.recyclerviewselectiondemo.recyclerview_selection;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.recyclerviewselectiondemo.Model.ContactModel;

import java.util.List;

import androidx.recyclerview.selection.ItemKeyProvider;

/**
 * A basic implementation of {@link ItemKeyProvider} for String items.
 */

public class ContactItemKeyProvider extends ItemKeyProvider<ContactModel> {

    private List<ContactModel> items;

    public ContactItemKeyProvider(int scope, List<ContactModel> items) {
        super(scope);
        this.items = items;
    }

    @Nullable
    @Override
    public ContactModel getKey(int position) {
        return items.get(position);
    }

    @Override
    public int getPosition(@NonNull ContactModel key) {
        try{
        return items.indexOf(key);}
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return RecyclerView.NO_POSITION;

//        RecyclerView.ViewHolder viewHolder = recyclerList.findViewHolderForItemId(key);
//        return viewHolder == null ? RecyclerView.NO_POSITION : viewHolder.getLayoutPosition();
    }
}
